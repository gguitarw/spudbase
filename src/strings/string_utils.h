﻿#pragma once

#include <string>
#include <sstream>
#include <vector>

namespace spud_base {
    namespace util {
        // splits the input string at every occurence of the delimiter
        inline std::vector<std::string> SplitString(const std::string& input, const char delimiter) {
            // https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/

            std::vector<std::string> split_strings;
            std::istringstream input_stream(input);
            std::string split;

            while (std::getline(input_stream, split, delimiter)) {
                if (split.length() == 0) {
                    // don't push back if the split string is empty
                    continue;
                }
                split_strings.push_back(split);
            }

            return split_strings;
        }

        // splits the input string at the first occurence of the delimiter
        inline std::pair<std::string, std::string> SplitStringAtFirst(const std::string& input, const char delimiter) {
            const int split_here = input.find_first_of(delimiter);
            return {
                input.substr(0, split_here),
                input.substr(split_here + 1, std::string::npos)
            };
        }

        inline std::string PadSpaces(std::string input, const size_t line_length) {
            for (size_t i = input.size(); i < line_length; ++i) {
                input.append(" ");
            }
            return input;
        }
    }
}
