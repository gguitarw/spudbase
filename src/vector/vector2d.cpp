﻿#include "vector2d.h"
#include <cmath>
#include "point/point.h"

using spud_base::point::Point;
using spud_base::vector::Vector2D;

Vector2D::Vector2D(const Point<double> p) : vector(p), start({ 0, 0 }), end(p) {}
Vector2D::Vector2D(double x, double y) : Vector2D(Point<double>(x, y)) {}

Vector2D::Vector2D(const Point<double> p1, const Point<double> p2) : vector(p2-p1), start(p1), end(p2) {}
Vector2D::Vector2D(double x_start, double y_start, double x_end, double y_end) : Vector2D({x_start, y_start}, {x_end, y_end}) {}

double Vector2D::Length() const {
    return sqrt(vector.x * vector.x + vector.y * vector.y);
}

Point<double> Vector2D::Midpoint() const {
    // return Point<double>((end.x + start.x) / 2,
    // (end.y + start.y) / 2);
    return {
        (end.x + start.x) / 2,
        (end.y + start.y) / 2
    };
}

Vector2D Vector2D::Normalized() const {
    Vector2D v(*this);
    const double length = v.Length();

    if (length != 0) {
        v.vector = v.vector / length;
        return v;
    } else {
        // length is 0
        return { 0, 0 };
    }
}

void Vector2D::Normalize() {
    *this = this->Normalized();
}

// returns the angle (radians) in the positive direction
double Vector2D::AngleRadians() const {
    const double pi = 3.1415926535897;
    double angle = atan2(vector.y, vector.x);
    if (angle < 0) {
        angle += 2 * pi;
    }
    return angle;
}

double Vector2D::AngleDegrees() const {
    const double pi = 3.1415926535897;
    return AngleRadians() * (180.0 / pi);
}

Vector2D Vector2D::Perpendicular() const {
    return { -vector.y, vector.x };
}

Point<double> Vector2D::Intersect(const Vector2D& vec) const {
    double ddd = (start.x - end.x) * (vec.start.y - vec.end.y)
                 - (start.y - end.y) * (vec.start.x - vec.end.x);
    double pre = (start.x * end.y) - (start.y * end.x);
    double pos = (vec.start.x * vec.end.y) - (vec.start.y * vec.end.x);
    return {
        (pre * -vec.vector.x - -vector.x * pos) / ddd,
        (pre * -vec.vector.y - -vector.y * pos) / ddd
    };
}

Vector2D Vector2D::Translate(const Point<double> translation) {
    start = start + translation;
    end = end + translation;

    return *this;
}

Vector2D Vector2D::Rotate(const double radians) {
    const Point<double> vec(this->vector);
    // https://en.wikipedia.org/wiki/Rotation_matrix#In_two_dimensions
    vector.x = vec.x * cos(radians) - vec.y * sin(radians);
    vector.y = vec.x * sin(radians) + vec.y * cos(radians);
    RecalculateEndPoint();
    return *this;
}
