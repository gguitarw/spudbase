﻿#pragma once
#include "point/point.h"

using spud_base::point::Point;

namespace spud_base {
    namespace vector {
        class Vector2D {
        public:
            Point<double> vector; // the vector itself
            Point<double> start; // tail
            Point<double> end; // head

            Vector2D(Point<double> p);
            Vector2D(double x, double y);
            Vector2D(double x_start, double y_start, double x_end, double y_end);
            Vector2D(Point<double> p1, Point<double> p2);
            ~Vector2D() = default;

            double Length() const; // magnitude (pop pop)
            Point<double> Midpoint() const;
            Vector2D Normalized() const; // returns a normalization of the current vector without modifying it
            void Normalize(); // normalizes the current vector
            double AngleRadians() const;
            double AngleDegrees() const;
            Vector2D Perpendicular() const;
            Point<double> Intersect(const Vector2D& vec) const;
            Vector2D Translate(Point<double> translation);
            Vector2D Rotate(const double radians); // rotates the vector about it's start point

            // vector addition (first vector's start point doesn't change)
            Vector2D operator+(const Vector2D& v) {
                vector = vector + v.vector;
                RecalculateEndPoint();
                return *this;
            };

            // vector subtraction (first vector's start point doesn't change)
            Vector2D operator-(const Vector2D& v) {
                vector = vector - v.vector;
                RecalculateEndPoint();
                return *this;
            }

            // scalar multiplication
            Vector2D operator*(const double& scalar) {
                vector = vector * scalar;
                RecalculateEndPoint();
                return *this;
            }

            // scalar division
            Vector2D operator/(const double& scalar) {
                vector = vector / scalar;
                RecalculateEndPoint();
                return *this;
            }

        private:
            void RecalculateEndPoint() {
                end = start + vector;
            }
        };
    }
}
