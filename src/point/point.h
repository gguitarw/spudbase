﻿#pragma once

#include <string>
#include <sstream>
#include <iomanip>

namespace spud_base {
    namespace point {
        template <typename T>
        class Point {
        // a two dimensional coordinate made up of x and y
        public:
            T x, y;

            Point() = default;
            Point(T X, T Y) : x(X), y(Y) {}

            // returns the coordinate components between two points
            inline Point<T> VectorTo(const Point<T>& another_point) {
                return Point<T>(another_point.x - this->x,
                                another_point.y - this->y);
            }

            // returns the distance from one point straight to another (double by default)
            template <typename D = double>
            inline D DistanceTo(const Point<T>& p) {
                return static_cast<D>(sqrt(pow(p.x - this->x, 2)
                                                + pow(p.y - this->y, 2)));
            }

            // convert from one type of point to another
            template <typename T2>
            Point(const Point<T2>& p) {
                x = static_cast<T>(p.x);
                y = static_cast<T>(p.y);
            }

            bool operator==(const Point<T>& p) const {
                return x == p.x && y == p.y;
            }

            Point<T> operator-(const Point<T>& p) const {
                return Point<T>(x - p.x,
                                y - p.y);
            }

            Point<T> operator+(const Point<T>& p) const {
                return Point<T>(x + p.x,
                                y + p.y);
            }

            template <typename T2>
            Point<T> operator*(const T2 mul) const {
                return Point<T>(x * mul,
                                y * mul);
            }

            template <typename T2>
            Point<T> operator/(const T2 div) const {
                return Point<T>(x / div,
                                y / div);
            }

            // string representation methods

            // return string in format: (x, y)
            std::string ToString(bool fixed = false) const {
                // return { "(" + std::to_string(x) + ", " + std::to_string(y) + ")" };
                // changed since to_string would leave trailing zeros and ugly output
                std::stringstream ss;
                ss << (fixed ? std::fixed : std::defaultfloat) << std::setprecision(6) << '(' << x << ", " << y << ')';
                return ss.str();
            }

            std::ostream& operator<<(std::ostream& out) const {
                out << "(" << x << ", " << y << ")";
                return out;
            }

            std::stringstream& operator<<(std::stringstream& out) const {
                out << "(" << x << ', ' << y << ")";
                return out;
            }
        };
    }

}
