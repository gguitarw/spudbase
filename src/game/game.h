﻿#pragma once

namespace spud_base {
    namespace game {
        enum GameMode {
            kModeStandard = 0,
            kModeTaiko = 1,
            kModeCtb = 2,
            kModeMania = 3
        };
         
        // consistent with the official api: https://github.com/ppy/osu-api/wiki#mods
        enum ModCombo {
            kModNone = 0,
            kModNoFail = 1,
            kModEasy = 2,
            kModNoVideo = 4, // Not used anymore, but can be found on old plays like Mesita on b/78239
            kModHidden = 8,
            kModHardRock = 16,
            kModSuddenDeath = 32,
            kModDoubleTime = 64,
            kModRelax = 128,
            kModHalfTime = 256,
            kModNightcore = 512, // Only set along with DoubleTime. i.e: NC only gives 576
            kModFlashlight = 1024,
            kModAutoplay = 2048,
            kModSpunOut = 4096,
            kModRelax2 = 8192, // Autopilot?
            kModPerfect = 16384, // Only set along with SuddenDeath. i.e: PF only gives 16416
            kModKey4 = 32768,
            kModKey5 = 65536,
            kModKey6 = 131072,
            kModKey7 = 262144,
            kModKey8 = 524288,
            kModkeyMod = kModKey4 | kModKey5 | kModKey6 | kModKey7 | kModKey8,
            kModFadeIn = 1048576,
            kModRandom = 2097152,
            kModLastMod = 4194304,
            kModFreeModAllowed = kModNoFail | kModEasy | kModHidden | kModHardRock | kModSuddenDeath | kModFlashlight |
              kModFadeIn | kModRelax | kModRelax2 | kModSpunOut | kModkeyMod,
            kModKey9 = 16777216,
            kModKey10 = 33554432,
            kModKey1 = 67108864,
            kModKey3 = 134217728,
            kModKey2 = 268435456
        };
    }
}
